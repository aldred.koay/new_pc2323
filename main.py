import fastapi
import uvicorn
from uvicorn import run

print("Hello fastapi")
api = fastapi.FastAPI()


@api.get("/")
def endpoint():
    return {"msg": "Hello Everyone", "list of ports": [8000, 9000, 6789]}


run(api, port=1900)
